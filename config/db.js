const mongoose = require("mongoose");

const dbURI =
  "mongodb+srv://chatdol:supersekali123@clusterapp-pq5cm.gcp.mongodb.net/groupstar01?retryWrites=true";

const options = {
  reconnectTries: Number.MAX_VALUE,
  poolSize: 10,
  useNewUrlParser: true
};

mongoose.connect(dbURI, options).then(
  () => {
    console.log("Database connection established!");
  },
  err => {
    console.log("Error connecting Database instance due to: ", err);
  }
);

// require any models

require("../models/Tracker");
require("../models/Car");