const Track = require("../models/Tracker");
const moment = require('moment');

//just a sample to track one car
exports.savePosition = (req, res) => {
 console.log('save position hit');
 let latitude = req.body.latitude;
 let longitude = req.body.longitude;

 var nDate = new Date().toLocaleString('en-US', {
    timeZone: 'Asia/Singapore'
  });

 let time = moment(nDate).format('MM/DD/YYYY HH:mm');

    let newPosition = new Track({"latitude": latitude, "longitude": longitude, "time":time});
    newPosition.save((err, result) => {
        if (err) {
        res.status(500).send(err);
        }
        res.status(201).json(result);
    });
};

//getTrackerHistory
exports.history = (req, res) => {
  Track.find({}, (err, result) => {
    if (err) {
      res.status(500).send(err);
    }
     res.status(200).json(result);
  });
};