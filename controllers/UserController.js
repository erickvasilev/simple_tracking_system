const User = require("../models/User");

exports.saveUser = (req, res) => {
  let newUser = new User(req.body);
  newUser.save((err, result) => {
    if (err) {
      res.status(500).send(err);
    }
    res.status(201).json(result);
  });
};


exports.getUser = (req, res) => {
  User.findById(req.params.userId, (err, result) => {
    if (err) {
      res.status(500).send(err);
    }
    res.status(200).json(result);
  });
};

exports.findUsers = (req, res) => {
let age = req.body.age;
let friends_name = req.body.friends_name;
User.find({'age' : age, 'friends': friends_name}, function (err, docs) { 
if(docs.length){
  res.status(200).json({"status": true,  "message": {"result": docs}  });
} else {
  res.status(200).json({
        "status": false,
        "message": 'not found'
      });
}  
});

};  
