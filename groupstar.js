const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const fs = require('fs');
const https = require('https');


// Certificate
const privateKey = fs.readFileSync('/etc/letsencrypt/live/parse.armadius.com/privkey.pem', 'utf8');
const certificate = fs.readFileSync('/etc/letsencrypt/live/parse.armadius.com/cert.pem', 'utf8');
const ca = fs.readFileSync('/etc/letsencrypt/live/parse.armadius.com/chain.pem', 'utf8');
const credentials = {
	key: privateKey,
	cert: certificate,
	ca: ca
};
const httpsServer = https.createServer(credentials, app);


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//connect to db
require("./config/db");

//Controller
const trackerController = require("./controllers/TrackerController");
const userController = require("./controllers/UserController");

//CORS
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();

});

//Routes
app.use("/public", express.static(__dirname + '/public'));

app.get('/', (req, res) => {
    res.send('Hello World!');
});

//question1 - GPS tracker
app
  .route("/tracker/position")
  .post(trackerController.savePosition)
  .get(trackerController.history);

//question2 - REST API for user
app
  .route("/profile")
  .post(userController.saveUser);

app
  .route("/profile/:userId")
  .get(userController.getUser);

app
  .route("/find-user")
  .post(userController.findUsers);

//create new server and open port
httpsServer.listen(7000, () => {
	console.log('Server running on port 7000');
}); 