const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const CarSchema = new Schema({
  carID: {
    type: String,
    required: true
  },
  createdOn: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model("Tracker", CarSchema);