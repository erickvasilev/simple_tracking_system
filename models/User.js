const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const UserSchema = new Schema({
  facebook_id: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
  },
   age: {
    type: String,
    required: true
  },
  friends: {
    type: String,   //we can store data as JSON, but I'll store 1 friend only for this test
    required: true
  }, 
  
});

module.exports = mongoose.model("Profile", UserSchema);